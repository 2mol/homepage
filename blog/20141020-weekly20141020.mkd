---
author: thoughtpolice
title: "GHC Weekly News - 2014/10/20"
date: 2014-10-20T14:14:23
tags: ghc news
---

Hi \*,

It's been a few weeks since the last message - and I apologize! We actually are changing the posting time to be **Friday** now, so hopefully this situation will be corrected preeeeetty quickly from this point forward, and hopefully will give better context over the course of a weekly discussion.

That said, let's begin!

 - We've seen plenty of changes to GHC itself in the past few weeks. Some of the highlights include:
   - Some changes to help make `Prelude` combinators fuse better. David Feuer has been leading a lot of this work, and it's been quite fruitful, with several new things now fusing (like `takeWhile`, `scanl`, `scanr`, and `mapAccumL`.
   - Relatedly, `Data.List.Inits` should be far faster thanks to David Feuer (ref: Phab:D329).
   - The testsuite driver now has preliminary support for Python 3 - which should be useful for platforms out there that sport it, and ones that will use it as the default eventually (such as Fedora 22, possibly).
   - Some of the initial work by Edward Yang to remove `HEAP_ALLOCED` from the GHC runtime system has landed. Briefly, `HEAP_ALLOCED` is a check the RTS uses to determine if some address is part of the dynamic heap - but the check is a bit costly. Edward's full patchset hopes to remove this with an 8% speedup or so on average.
   - GHC now has a new macro, `__GLASGOW_HASKELL_PATCHLEVEL__`, which will allow you to determine the point-level release of the GHC you're using. This has been a requested feature in the past we were a little hesitant of adding, but Herbert went through and did it for us. (Ref: Phab:D66)
   - Template Haskell now supports `LINE` pragmas, thanks to Eric Mertens (ref: Phab:D299).
   - Sergei Trofimovich revived `libbfd` debugging support for the runtime system linker, which should be of use to several daring souls out there (ref: Phab:D193).
   - Several patches from Gintautas Miliauskas has improved the usability of msys and the testsuite on Windows - and he's not done yet!
   - A few improvements to the x86 code generator were committed by Reid Barton and Herbert Valerio Riedel, improving size/space for certain cases (ref: Phab:D320, Phab:D163).
 and more besides that, including some linker improvements, and general cleanups as usual.

 - The mailing list has been busy (as usual), with some discussions including:
   - Austin posted some discussion about the tentative 7.10.1 plans - we're still hoping these are accurate, so take note! **We hope to freeze mid-November, and release Feburary 2015** [1][]
   - Austin also called for some feedback: GHC HQ has become convinced a 7.8.4 release is needed to fix some showstoppers - so please let him know soon if you're totally incapable of using 7.8 for something! [2][]
   - Alan Zimmerman has asked for some feedback on his proposed "AST Annotations", which will hopefully allow GHC API clients to add richer annotations to GHC's syntactic representations. The motivation is for refactoring tools like HaRe - and I'm sure all input would be appreciated. [3][]
   - Chris done sparked off a discussion about making GHCi awesomer, and I'm sure everyone can appreciate that! In particular, Chris wanted to discuss programmatic means of controlling GHCi itself, and naturally we need to ask - is the current API not enough, and why? [4][]
   - Yuras Shumovich has implemented a proposal for allowing the Haskell FFI to support C structures natively as return values - this involves interfacing with C ABI rules to properly support structure layout. While Yuras has an initial implementation in Phab:D252, some questions about the feature - including its implementation complexity - remain before it gets merged. [5][]
   - Richard Eisenberg made a modest request: can Phabricator patches have a 'roadmap', so people can instruct reviewers **how** to read a diff? The answer: yes, and it should be relatively easy to implement, and Austin can do so Real Soon Now™. [6][]
   - Ben Gamari started a big discussion about one-shot event semantics in the I/O manager, with a lot of replies concerning not only the bug, but machines to test the actual change on. With any luck, Ben's fix for the I/O manager and a test machine should come quickly enough. [7][]
   - Herbert Valerio Riedel opened an RFC: Should we look into using AsciiDoc for GHC? Historically, GHC's documentation has been written using DocBook, a verbose but very precise and unambiguous documentation format. However, AsciiDoc offers a much nicer markup language, while retaining DocBook support. In short, it looks like GHC may get a much more clean user manual soon. [8][]
   - Yuras opened another discussion: Should we namespace proposals we create on our wiki? What seems uncontroversial can lead to surprising discussion, and the results were mixed this time it seems. [9][]
   - Geoff Mainland stepped up and fixed Data Parallel Haskell to work with a new version of `vector` and GHC. Austin had disabled DPH a few weeks prior due to its difficulty to upgrade, and divergent source trees. With 7.10, GHC will hopefully ship a more modern `vector` and `dph` to boot.
   - Austin asks: can we warn on tabs by default for GHC 7.10? It's an easy change and a minor one - but we should at least ask first. Vote now! [10][]
   - Philip Hölzenspies opens up a discussion about Uniques in GHC, and their impact on the compilers current design. Philip has a hopeful design to redo `Unique` values in GHC, and a patch to support it: Phab:D323. [11][]
   - Richard Eisenberg asks: can we somehow integrate GitHub into our development process? While GitHub doesn't have as many nice tools as something like Phabricator, it has a very high inertia factor, and Richard is interested in making the "first step" as easy as possible for newbies. Discussions about Phab-GitHub integrations were afoot, as well as general discussion about contributor needs. There were a lot of points brought up, but the conversation has slightly dried up now - but will surely be revived again. [12][]

And now, look at all these tickets we closed! Including: #9658, #9094, #9356, #9604, #9680, #9689, #9670, #9345, #9695, #9639, #9296, #9377, #9184, #9684.

[1]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006518.html
[2]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006713.html
[3]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006482.html
[4]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006771.html
[5]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006616.html
[6]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006719.html
[7]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006682.html
[8]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006599.html
[9]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006730.html
[10]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006769.html
[11]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006546.html
[12]: http://www.haskell.org/pipermail/ghc-devs/2014-October/006523.html
