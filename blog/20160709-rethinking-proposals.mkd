---
author: bgamari
title: "Rethinking GHC's approach to managing proposals"
date: 2016-07-09T20:41:08
tags: proposals
---

Recently there has been a [fair bit](https://www.reddit.com/r/haskell/comments/4oyxo2/blog_contributing_to_ghc/) of [discussion](https://www.reddit.com/r/haskell/comments/4isua9/ghc_development_outsidein/) around the
mechanisms by which proposed changes to GHC are evaluated. While we have
something of a formal proposal [protocol](https://ghc.haskell.org/trac/ghc/wiki/WorkingConventions/AddingFeatures), it is not clearly
documented, inconsistently applied, and may be failing to serve a
significant fraction of GHC's potential contributor pool.

Over the last few weeks, I have been doing a fair amount of reading,
thinking, and discussing to try to piece together a proposal scheme
which better serves our community.

The [resulting proposal](https://github.com/ghc-proposals/ghc-proposals/pull/1/files?short_path=14d66cd#diff-14d66cda32248456a5f223b6333c6132) is strongly inspired by the [RFC process](https://github.com/rust-lang/rfcs) in
place in the Rust community, the leaders of which have thought quite
hard about fostering community growth and participation. While no
process is perfect, I feel like the Rust process is a good starting
point for discussion, offering enough structure to guide new
contributors through the process while requiring only a modest
investment of developer time.

To get a sense for how well this will work in our community, I propose
that we attempt to self-host the proposed process. To this end I have
setup a `ghc-proposals` [repository](https://github.com/ghc-proposals/ghc-proposals) and opened a pull request for
discussion of the [process proposal](https://github.com/ghc-proposals/ghc-proposals/pull/1/files?short_path=14d66cd#diff-14d66cda32248456a5f223b6333c6132).
           
Let's see how this goes.

Cheers,

~ Ben
