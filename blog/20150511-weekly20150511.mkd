---
author: thoughtpolice
title: "GHC Weekly News - 2015/05/11"
date: 2015-05-11T14:49:11
tags: ghc news
---

Hi \*,

It's been a few weeks since the last news bulletin - this is the result of mostly quietness on behalf of the list and developers, and some sickness on behalf of your editor for several days there. But now there's actually some things to write here!

The past few weeks, GHC HQ has been having some quiet meetings mostly about bugfixes for a 7.10.2 release - as well as noodling about compiler performance. Austin has begun compiling his preliminary notes on the wiki, under the [wiki:CompilerPerformance] page, where we'll be trying to keep track of the ongoing performance story. Hopefully, GHC 7.12.1 will boast a bit better performance numbers.

There are a lot of users who are interested in this particular pain point, so please file tickets and CC yourself on bugs (like #10370), or feel free to help out!

## 7.10.2 status

There's been a bit of chatter about the lists about something on many peoples mind: the release of GHC 7.10.2. Most prominently, Mark Lentczner popped in to ask when the next GHC release will happen - in particular, he'd like to make a Haskell Platform release in lockstep with it (see below for a link to Mark's email).

Until recently, the actual desire for 7.10.2 wasn't totally clear, and at this point, GHC HQ hasn't firmly committed to the 7.10.2 release date. But if milestone:7.10.2 is any indicator, we've already closed over three dozen bugs, several of them high priority - and they keep coming in. So it seems likely people will want these fixes in their hands relatively soon.

Just remember: **if you need a fix for 7.10.2**, or have a bug you need us to look at, please email the `ghc-devs` list, file a ticket, and get our attention! Just be sure to set the milestone to 7.10.2.

## List chatter

 - Herbert Valerio Riedel opened an RFC about a regression in GHC 7.10 relating to the update to Unicode 7. Any input from users of international languages or unicode users would be appreciated! <https://mail.haskell.org/pipermail/ghc-devs/2015-May/008930.html>

 - Herbert Valerio Riedel also asked about a new C pre-processor implementation for GHC - but in particular, adopting the extant `cpphs` into the GHC codebase for this task itself. <https://mail.haskell.org/pipermail/ghc-devs/2015-May/008934.html>

 - Austin Seipp emailed `ghc-devs` about the HCAR report, for which the GHC entry is due May 17th! Developers should get their edits in quickly. <https://mail.haskell.org/pipermail/ghc-devs/2015-May/008939.html>

 - Joachim Breitner asks if the branchless implementation for our literal cases are worth it for their complexity. There were some interesting responses, including some remarks on the V8 JavaScript compiler. <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008852.html>

 - Niklas Hambüchen announced that he's backported the recent lightweight stack-trace support in GHC HEAD to GHC 7.10 and GHC 7.8 - meaning that users of these stable release can have informative call stack traces, even without profiling! FP Complete was interested in this feature, so they'd probably love to hear user input. <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008862.html>

 - David Terei has written up a proposal on reconciling the existence of Roles with Safe Haskell, which caused us a lot of problems during the 7.8 release cycle. In particular, concerning the ability to break module abstractions and requiring programmers to safeguard abstractions through careful use of roles - and David's written a proposal to address that. <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008902.html>

 - Mark Lentczner started a thread about the 7.10.2 release schedule - because this time, he wants to do a concurrent Haskell Platform release! The thread ended up with a good amount of discussion concerning if 7.10.2 is even needed - but at this rate, it looks like it will ship sometime soon. <https://mail.haskell.org/pipermail/ghc-devs/2015-May/008904.html>

 - Mateusz Kowalczyk posted to `ghc-devs` hoping to get some help with a tricky, long-standing issue: #4012, which concerns the determinism of GHC binaries. It turns out GHC isn't entirely deterministic when it calculates package IDs, meaning things get really bad when you mix prebuilt binary packages for systems. This in particular has become a real problem for the Nix package manager and users of Haskell applications. Mateusz asks if anyone would be willing to help look into it - and a lot of people would appreciate the help! <https://mail.haskell.org/pipermail/ghc-devs/2015-May/008992.html>

## Noteworthy commits

 - Commit f2d1b7fcbbc55e33375a7321222a9f4ee189aa38 - Support unboxing for GADT product types.

 - Commit 51af102e5c6c56e0987432aa5a21fe10e24090e9 - Better hints when RTS options are not available.

 - Commit 524ddbdad5816f77b7b719cac0671eebd3473616 - Make sure `GHC.List.last` is memory-efficient.

 - Commit a1275a762ec04c1159ae37199b1c8f998a5c5499 - Improve improvement in the constraint solver.

 - Commit 4efa421327cf127ebefde59b2eece693e37dc3c6 - Permit empty closed type families.

 - Commit 477f514f6ebcf783810da93e2191e4b6ea65559b - rts: add "-no-rtsopts-suggestions" option

 - Commit cf7573b8207bbb17c58612f3345e0b17d74cfb58 - More accurate allocation stats for :set +s

 - Commit c4e8097ea8dd6e43eae7aadd6bae7e13272ba74d - Bump base version to `4.8.2.0`

 - Commit 0bbc2ac6dae9ce2838f23a75a6a989826c06f3f5 - Use the gold linker for aarch64/linux (#9673)

 - Commit 1e8c9b81a819da8eb54405a029fc33a9f5220321 - Enable SMP and GHCi support for AArch64

## Closed tickets

#10293, #10273, #10021, #10209, #10255, #10326, #9745, #10314, #8928, #8743, #10182, #10281, #10325, #10297, #10292, #10304, #10260, #9204, #10121, #10329, #9920, #10308, #10234, #10356, #10351, #10364, #9564, #10306, #10108, #9581, #10369, #9673, #10288, #10260, #10363, #10315, #10389, #9929, #10384, #10382, #10400, #10256, #10254, #10277, #10299, #10268, #10269, #10280, #10312, #10209, #10109, #10321, #10285, #9895, #10395, #10263, #10293, #10210, #10302, #10206, #9858, #10045, and #9840.
