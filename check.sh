#!/usr/bin/env bash

# docs/ seems to be a URL rewrite rule provided by haskell.org
# dist/mac_frameworks appears to be lost to history
linkchecker _site \
  --ignore-url 'docs/' \
  --ignore-url=dist/mac_frameworks \

