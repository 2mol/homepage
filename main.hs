{-# LANGUAGE OverloadedStrings #-}

import Data.Char
import Data.List
import Data.Maybe
import Data.Monoid
import Numeric

import System.FilePath

import Data.Time
import Data.Time.Format

import Hakyll
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as HA
import qualified Text.Blaze.Html.Renderer.String as H
import Text.Pandoc
import Text.Pandoc.Walk

import Types

main :: IO ()
main = hakyll rules

rules :: Rules ()
rules = do
    match "ghc.css" $ do
        route     idRoute
        compile   copyFileCompiler

    match "images/*" $ do
        route     idRoute
        compile   copyFileCompiler

    match "download_*.shtml" $ do
        let ctx' = titleField <> tarballsField <> downloadsUrlField
                <> isoDateField <> ctx
            isoDateField = functionField "iso-date" $ \_ item -> do
                date <- getItemUTC defaultTimeLocale (itemIdentifier item)
                let format = iso8601DateFormat (Just "%H:%M:%S")
                return $ formatTime defaultTimeLocale format date
            titleField = functionField "title" $ \_ item -> do
                version <- fromMaybe "???" <$> getMetadataField (itemIdentifier item) "version"
                return $ "GHC "++version++" download"

        route   $ setExtension "html"
        compile $ getResourceBody
              >>= applyAsTemplate ctx'
              >>= loadAndApplyTemplate "templates/default.html" ctx'
              >>= relativizeUrls

    match "*.shtml" $ do
        route   $ setExtension "html"
        compile $ getResourceBody
              >>= applyAsTemplate ctx
              >>= loadAndApplyTemplate "templates/default.html" ctx
              >>= relativizeUrls

    match "*.mkd" $ do
        route   $ setExtension "html"
        compile $ pandocCompilerWithTransform defaultHakyllReaderOptions defaultHakyllWriterOptions pandocTransforms
              >>= loadAndApplyTemplate "templates/default.html" ctx
              >>= relativizeUrls

    match "blog/*.mkd" $ do
        route   $ setExtension "html"
        compile $ pandocCompilerWithTransform defaultHakyllReaderOptions defaultHakyllWriterOptions pandocTransforms
              >>= loadAndApplyTemplate "templates/blog-post.html" ctx
              >>= loadAndApplyTemplate "templates/default.html" ctx
              >>= relativizeUrls

    create ["blog.html"] $ do
            route idRoute
            compile $ loadAll "blog/*.mkd"
                    >>= postList
                    >>= loadAndApplyTemplate "templates/blog-list.html" ctx
                    >>= loadAndApplyTemplate "templates/default.html" (field "title" (const $ return "Developer Blog Posts") <> ctx)
                    >>= relativizeUrls

    match "templates/*" $ compile templateCompiler
    match "partials/*" $ compile getResourceString
    match "files.index" $ compile getResourceString

  where
      ctx = snippetField <> defaultContext


postList :: [Item String] -> Compiler (Item String)
postList posts =
    chronological posts
    >>= return . reverse
    >>= mapM (loadAndApplyTemplate "templates/blog-list-item.html" ctx)
    >>= makeItem . mconcat . map itemBody
  where
    ctx =
        renderTagList' "tagsList" (const $ fromFilePath "error/404")
        <> dateField "date" "%e. %B %Y"
        <> dateField "date-iso" "%F"
        <> defaultContext
--
-- | Render tags as HTML list with links
renderTagList' :: String
               -- ^ Destination key
               -> (String -> Identifier)
               -- ^ Produce a link for a tag
               -> Context String
renderTagList' destination makeUrl =
    field destination $ \item->renderTags <$> getTags (itemIdentifier item)
  where
    renderTags :: [String] -> String
    renderTags = H.renderHtml . mconcat . map (H.li . H.toHtml)

pandocTransforms :: Pandoc -> Pandoc
pandocTransforms = linkifyIssues

linkifyIssues :: Pandoc -> Pandoc
linkifyIssues = walk linkify
  where
    linkify :: [Inline] -> [Inline]
    linkify (Str s : xs) = linkifyStr s ++ linkify xs
    linkify (x:xs) = x : linkify xs
    linkify [] = []

    linkifyStr :: String -> [Inline]
    linkifyStr ('#':s)
      | [(n, rest)] <- reads s
      = issueLink n : linkifyStr rest
    linkifyStr (c:s) = Str [c] : linkifyStr s
    linkifyStr [] = []

    issueLink :: Int -> Inline
    issueLink n = Link nullAttr [Str $ "#"++show n] (url, "")
      where url = "https://gitlab.haskell.org/ghc/ghc/issues/" ++ show n

downloadsUrlField :: Context a
downloadsUrlField = functionField "downloads_url" $ \_ item -> do
    mversion <- getMetadataField (itemIdentifier item) "version"
    case mversion of
      Nothing -> fail $ "downloadsUrlField: No version"
      Just version -> return $ rootUrl </> version

tarballsField :: Context a
tarballsField = functionField "tarballs" $ \args item -> do
    files <- fmap read $ loadBody "files.index" :: Compiler [DownloadFile]
    let ident = itemIdentifier item
        uhOh err = do
            unsafeCompiler $ putStrLn $ "Warning: " <> show ident <> ": " <> err
            return $ H.renderHtml $ H.toHtml err
    root <- fromMaybe "" <$> getMetadataField ident "bindist-root"
    filename <- case args of
                  [filename] -> return filename
                  _          -> uhOh "Invalid argument list for $file"

    mversion <- getMetadataField ident "version"
    case mversion of
      Nothing -> uhOh $ "No file for " <> filename
      Just version ->
        let isMyFile f = (version </> "ghc-" <> version <> "-" <> filename <> ".") `isPrefixOf` filePath f
            toFileContent f = H.li $ do
                downloadLink (filePath f) $ H.toHtml (takeFileName $ filePath f)
                " (" <> H.toHtml (showFFloat (Just 1) (realToFrac (fileSize f) / 1024 / 1024) "") <> " MB"
                case fileSignature f of
                  Just sig -> ", " >> downloadLink sig "sig"
                  Nothing  -> mempty
                ")"
        in case filter isMyFile files of
             [] -> uhOh $ "No files for " <> filename
             files' -> return $ H.renderHtml $ H.ul $ foldMap toFileContent files'

downloadLink :: FilePath -> H.Html -> H.Html
downloadLink path body = H.a H.! HA.href (H.stringValue $ downloadUrl path) $ body

downloadUrl path = rootUrl <> "/" <> path

rootUrl = "https://downloads.haskell.org/~ghc"
